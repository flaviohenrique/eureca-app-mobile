package cin.ufpe.br.eureca_app_mobile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Toast;

import org.json.JSONObject;

/**
 * An activity representing a single Question detail screen. This
 * activity is only used on handset devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a {@link QuestionListActivity}.
 * <p/>
 * This activity is mostly just a 'shell' activity containing nothing
 * more than a {@link QuestionDetailFragment}.
 */
public class QuestionDetailActivity extends AppCompatActivity {

    private boolean checked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            Bundle arguments = new Bundle();
            arguments.putString(QuestionDetailFragment.ARG_ITEM_ID,
                    getIntent().getStringExtra(QuestionDetailFragment.ARG_ITEM_ID));
            QuestionDetailFragment fragment = new QuestionDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.question_detail_container, fragment)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            NavUtils.navigateUpTo(this, new Intent(this, QuestionListActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_yes:
                if (checked)
                    // Yes
                    this.checked = true;
                    break;
            case R.id.radio_no:
                if (checked)
                    // No
                    this.checked = false;
                    break;
        }
    }

    public void sendMessage(View view) {
        // Do something in response to button click
        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        String item_id = bundle.getString("item_id");
        String token = bundle.getString("token");

        new AnswerASync().execute(item_id, token);
    }

    class AnswerASync extends AsyncTask<String, String, JSONObject> {
        JSONParser jsonParser = new JSONParser();

        private ProgressDialog pDialog;

        private static final String ANSWER_URL = "https://eureca-app-web.herokuapp.com/answers/save/";

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(QuestionDetailActivity.this);
            pDialog.setMessage("Attempting login...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... args) {
            try {

                String response = checked ? "true" : "false";

                JSONObject params = new JSONObject();
                params.put("question", args[0]);
                params.put("response", response);

                Log.d("request answer", "starting");

                jsonParser.setToken(args[1]);
                JSONObject json = (JSONObject) jsonParser.makeHttpRequest(
                        ANSWER_URL, "POST", params);

                if (json != null) {
                    Log.d("JSON result", json.toString());

                    return json;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(JSONObject json) {
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.dismiss();
            }

            if (json != null) {
                Toast toast;
                String text;
                Context context = QuestionDetailActivity.this;
                if (json.has("created_at")) {
                    text = "Answer saved!";
                    toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
                } else {
                    text = "Answer was not saved, please, try again.";
                    toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
                }

                toast.show();
            }
        }
    }
}
