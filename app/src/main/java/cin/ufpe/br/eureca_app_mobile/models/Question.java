package cin.ufpe.br.eureca_app_mobile.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Flavio on 4/12/16.
 */
public class Question {
    /**
     * An array of sample (question) items.
     */
    public static List<QuestionItem> ITEMS = new ArrayList<QuestionItem>();

    /**
     * A map of sample (question) items, by ID.
     */
    public static Map<String, QuestionItem> ITEM_MAP = new HashMap<String, QuestionItem>();

    public static void addItem(QuestionItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    /**
     * A question item representing a piece of content.
     */
    public static class QuestionItem {
        public String id;
        public String title;
        public String description;

        public QuestionItem(String id, String title, String description) {
            this.id = id;
            this.title = title;
            this.description = description;
        }

        @Override
        public String toString() {
            return title;
        }
    }
}
